module.exports = {
  entry: "./src/index.js",
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "eslint-loader"
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      },
      {
        test: /\.css$/,
        exclude: /\.module\.css$/,
        use: [
          "style-loader", 
          { loader: "css-loader", options: { importLoaders: 1 } }, 
          "postcss-loader"
        ]
      },
      {
        test: /\.module\.css$/,
        use: [
          "style-loader", 
          {
            loader: "css-loader",
            options: { 
              importLoaders: 1,
              modules: true, 
              localIdentName: "[folder]__[local]--[hash:base64:5]"
            }
          }, 
          "postcss-loader"
        ]
      },
      {
        test: /\.(svg|png|jpg|jpeg|pdf)$/,
        loader: "file-loader",
        query: {
          name: "[name].[hash:8].[ext]",
          outputPath: "static/media",
          publicPath: "/static/media/"
        }
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: "file-loader",
        query: {
          name: "[name].[hash:8].[ext]",
          outputPath: "static/fonts",
          publicPath: "/static/fonts/"
        }
      }
    ],
  }
};
