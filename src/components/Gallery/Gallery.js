import React from "react";

import cs from "./Gallery.module.css";
import data from "./data";

const Gallery = () => {
  return (
    <section className={cs["gallery"]}>
      {data.map((img, index) => {
        return (
          <figure key={index} className={`${cs["item"]} ${cs[`item--${index+1}`]}`}>
            <img
              src={require(`../../img/${img}`)}
              alt={`Gallery image ${index}`}
              className={cs["img"]} />
          </figure>
        );
      })}
    </section>
  );
};

export default Gallery;
