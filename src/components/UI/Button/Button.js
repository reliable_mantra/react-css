import React from "react";
import PropTypes from "prop-types";

import cs from "./Button.module.css";

const Button = ({children, className}) => {
  return <button className={`${cs["btn"]} ${className}`}>{children}</button>;
};

Button.propTypes = {
  children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
  ]),
  className: PropTypes.string
};

export default Button;
