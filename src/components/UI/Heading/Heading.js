import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import cs from "./Heading.module.css";

const Heading = ({children, className, size, dark}) => {
  const Tag = `h${size}`;
  const classNames = cn(
    cs[`heading-${size}`],
    {
      [cs["heading-4--light"]]: size === "2" || size === "4" && dark,
      [cs["heading-4--dark"]]: size === "2" || size === "4" && dark
    }
  );

  return <Tag className={`${className} ${classNames}`}>{children}</Tag>;
};

Heading.propTypes = {
  children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
  ]),
  className: PropTypes.string,
  size: PropTypes.string,
  dark: PropTypes.bool
};

export default Heading;
