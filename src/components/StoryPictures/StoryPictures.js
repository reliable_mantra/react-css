import React from "react";

import cs from "./StoryPictures.module.css";
import story_1 from "../../img/story-1.jpeg";
import story_2 from "../../img/story-2.jpeg";

const StoryPictures = () => {
  return (
    <div className={cs["story__pictures"]}>
      <img src={story_1} alt="Couple with new house" className={cs["img-1"]} />
      <img src={story_2} alt="New house" className={cs["img-2"]} />
    </div>
  );
};

export default StoryPictures;
