import React from "react";

import cs from "./Header.module.css";
import Heading from "../UI/Heading/Heading";
import Button from "../UI/Button/Button";
import logo from "../../img/logo.png";
import logoBbc from "../../img/logo-bbc.png";
import logoForbes from "../../img/logo-forbes.png";
import logoTechCrunch from "../../img/logo-techcrunch.png";
import logoBi from "../../img/logo-bi.png";

const Header = () => {
  return (
    <header className={cs["header"]}>
      <img src={logo} alt="Nexter logo" className={cs["logo"]} />
      <Heading size="3">Your own home:</Heading>
      <Heading size="1">The ultimate personal freedom</Heading>
      <Button className={cs["btn"]}>View our properties</Button>
      <div className={cs["seenon-text"]}>Seen on</div>
      <div className={cs["seenon-logos"]}>
        <img src={logoBbc} alt="Seen on logo 1" />
        <img src={logoForbes} alt="Seen on logo 2" />
        <img src={logoTechCrunch} alt="Seen on logo 3" />
        <img src={logoBi} alt="Seen on logo 4" />
      </div>
    </header>
  );
};

export default Header;
