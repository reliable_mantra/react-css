import React from "react";
import PropTypes from "prop-types";

import cs from "./Home.module.css";
import sprite from "../../../img/sprite.svg";
import Heading from "../../UI/Heading/Heading";
import Button from "../../UI/Button/Button";

const Home = ({img, name, location, rooms, area, price}) => {
  return (
    <div className={cs["home"]}>
      <img src={require(`../../../img/${img}`)} alt={name} className={cs["img"]} />
      <svg className={cs["like"]}>
        <use xlinkHref={`${sprite}#icon-heart-full`}></use>
      </svg>
      <Heading size="5" className={cs["name"]}>{name}</Heading>
      <div className={cs["location"]}>
        <svg>
            <use xlinkHref={`${sprite}#icon-map-pin`}></use>
        </svg>
        <p>{location}</p>
      </div>
      <div className={cs["rooms"]}>
        <svg>
            <use xlinkHref={`${sprite}#icon-profile-male`}></use>
        </svg>
        <p>{rooms} rooms</p>
      </div>
      <div className={cs["area"]}>
        <svg>
            <use xlinkHref={`${sprite}#icon-expand`}></use>
        </svg>
        <p>{area} m</p>
      </div>
      <div className={cs["price"]}>
        <svg>
            <use xlinkHref={`${sprite}#icon-key`}></use>
        </svg>
        <p>{price}</p>
      </div>
      <Button className={cs["btn"]}>Contact realtor</Button>
    </div>
  );
};

Home.propTypes = {
  img: PropTypes.string,
  name: PropTypes.string,
  location: PropTypes.string,
  rooms: PropTypes.string,
  area: PropTypes.string,
  price: PropTypes.string
};

export default Home;
