import React from "react";

import cs from "./Homes.module.css";
import data from "./data";
import Home from "./Home/Home";

const Homes = () => {
  return (
    <section className={cs["homes"]}>
      {data.map((home, index) => {
        return (
          <Home
            key={index}
            img={home.img}
            name={home.name}
            location={home.sold}
            rooms={home.rooms}
            area={home.area}
            price={home.price}  />
        );
      })}
    </section>
  );
};

export default Homes;
