export default [
  {
    img: "realtor-1.jpeg",
    name: "Erik Feinman",
    sold: "245"
  },
  {
    img: "realtor-2.jpeg",
    name: "Kim Brown",
    sold: "212"
  },
  {
    img: "realtor-3.jpeg",
    name: "Toby Ramsey",
    sold: "198"
  }
];
