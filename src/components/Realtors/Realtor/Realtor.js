import React from "react";
import PropTypes from "prop-types";

import cs from "./Realtor.module.css";
import Heading from "../../UI/Heading/Heading";

const Realtor = ({img, name, sold}) => {
  return (
    <div className={cs["realtor"]}>
      <img src={require(`../../../img/${img}`)} alt={name} className={cs["img"]} />
        <div className={cs["details"]}>
          <Heading size="4" light>{name}</Heading>
          <p className={cs["sold"]}>{sold} houses sold</p>
        </div>
    </div>
  );
};

Realtor.propTypes = {
  img: PropTypes.string,
  name: PropTypes.string,
  sold: PropTypes.string
};

export default Realtor;
