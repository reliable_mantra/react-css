import React from "react";

import cs from "./Realtors.module.css";
import data from "./data";
import Heading from "../UI/Heading/Heading";
import Realtor from "./Realtor/Realtor";

const Realtors = () => {
  return (
    <div className={cs["realtors"]}>
      <Heading size="3">Top 3 Realtors</Heading>
      <div className={cs["list"]}>
        {data.map((realtor, index) => {
          return (
            <Realtor
              key={index}
              img={realtor.img}
              name={realtor.name}
              sold={realtor.sold} />
          );
        })}
      </div>
    </div>
  );
};

export default Realtors;
