import React from "react";

import cs from "./Footer.module.css";

const Footer = () => {
  return (
    <footer className={cs["footer"]}>
      <ul className={cs["nav"]}>
        <li className={cs["item"]}><a href="#" className={cs["link"]}>Find your dream home</a></li>
        <li className={cs["item"]}><a href="#" className={cs["link"]}>Request proposal</a></li>
        <li className={cs["item"]}><a href="#" className={cs["link"]}>Download home planner</a></li>
        <li className={cs["item"]}><a href="#" className={cs["link"]}>Contact us</a></li>
        <li className={cs["item"]}><a href="#" className={cs["link"]}>Submit your property</a></li>
        <li className={cs["item"]}><a href="#" className={cs["link"]}>Come work with us!</a></li>
      </ul>
      <p className={cs["copyright"]}>
        &copy; Copyright 2017 by Jonas Schmedtmann. Feel free to use this project for your own purposes. This does NOT
        apply if you plan to produce your own course or tutorials based on this project.
      </p>
    </footer>
  );
};

export default Footer;
