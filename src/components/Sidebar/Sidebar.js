import React from "react";

import cs from "./Sidebar.module.css";

const Sidebar = () => {
  return (
    <div className={cs["sidebar"]}>
      <button className={cs["nav-btn"]} />
    </div>
  );
};

export default Sidebar;
