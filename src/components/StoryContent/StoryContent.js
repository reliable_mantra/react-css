import React from "react";

import cs from "./StoryContent.module.css";
import Heading from "../UI/Heading/Heading";
import Button from "../UI/Button/Button";

const StoryContent = () => {
  return (
    <div className={cs["story__content"]}>
      <Heading size="3" className="mb-sm">Happy Customers</Heading>
      <Heading size="2" dark className="mb-md">&ldquo;The best decision of our lives&rdquo;</Heading>
      <p className={cs["text"]}>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tenetur distinctio necessitatibus pariatur voluptatibus. Quidem consequatur harum volupta!
      </p>
      <Button>Find your own home</Button>
    </div>
  );
};

export default StoryContent;
