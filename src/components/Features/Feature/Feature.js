import React from "react";
import PropTypes from "prop-types";

import cs from "./Feature.module.css";
import sprite from "../../../img/sprite.svg";
import Heading from "../../UI/Heading/Heading";

const Feature = ({icon, title, description}) => {
  return (
    <div className={cs["feature"]}>
      <svg className={cs["icon"]}>
        <use xlinkHref={`${sprite}${icon}`}></use>
      </svg>
      <Heading size="4" dark>{title}</Heading>
      <p className={cs["description"]}>{description}</p>
    </div>
  );
};

Feature.propTypes = {
  icon: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string
};

export default Feature;
