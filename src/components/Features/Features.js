import React from "react";

import cs from "./Features.module.css";
import data from "./data";
import Feature from "./Feature/Feature";

const Features = () => {
  return (
    <section className={cs["features"]}>
      {data.map((feature, index) => {
        return (
          <Feature
            key={index}
            icon={feature.icon}
            title={feature.title}
            description={feature.description} />
        );
      })}
    </section>
  );
};

export default Features;
