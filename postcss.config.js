module.exports = {
  ident: 'postcss',
  plugins: {
    "postcss-normalize": {},
    "postcss-preset-env": {
      autoprefixer: {
        flexbox: 'no-2009',
      },
      stage: 3,
    },
    "postcss-flexbugs-fixes": {},
    "postcss-nested": {},
    "postcss-custom-media": {
      importFrom: "src/css/_mq.css"
    }
  },
  map: true
};